<?php

namespace HrCart;

use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\ActivateContext;
use Shopware\Components\Plugin\Context\DeactivateContext;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UninstallContext;

class HrCart extends Plugin
{
    /**
     * @param ActivateContext $context
     *
     * @return void
     */
    public function activate(ActivateContext $context): void
    {
        $this->clearCache($context);
    }

    /**
     * @param DeactivateContext $context
     *
     * @return void
     */
    public function deactivate(DeactivateContext $context): void
    {
        $this->resetCustomerGroup();
        $this->clearCache($context);
    }

    /**
     * @param UninstallContext $context
     *
     * @return void
     */
    public function uninstall(UninstallContext $context): void
    {
        $this->resetCustomerGroup();
        $this->clearCache($context);
    }

    /**
     * @param InstallContext $context
     *
     * @return void
     */
    private function clearCache(InstallContext $context): void
    {
        $context->scheduleClearCache(InstallContext::CACHE_LIST_ALL);
    }

    private function resetCustomerGroup(): void
    {
        $connection = $this->container->get('dbal_connection');
        if (!$connection) {
            return;
        }
        $sql = "UPDATE s_user SET customergroup='EK' WHERE customergroup='VF'";
        $connection->executeStatement($sql);
    }
}
