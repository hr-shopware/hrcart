{extends file="parent:frontend/account/index.tpl"}

{* Newsletter settings *}
{block name="frontend_account_index_newsletter_settings_content"}
    <div class="panel--body is--wide">
        <form name="frmRegister" method="post" action="{url action=saveNewsletter}">
            <fieldset>
                <input type="checkbox" name="newsletter" value="1" id="newsletter" data-auto-submit="true"
                       {if $sUserData.additional.user.newsletter}checked="checked"{/if} />
                <label for="newsletter">{s name="AccountLabelWantNewsletter"}{/s}</label>
                {if $hrCart.groupFree}
                    <div>
                        <br/>
                        <strong>{s name="order_free_shipping" namespace="frontend/hr_cart/account"}{/s}</strong>
                        {s name="shipping_free_info_text" namespace="frontend/hr_cart/account"}{/s}
                        {s name="shipping_free_info_text_2" namespace="frontend/hr_cart/account"}{/s}
                    </div>
                {/if}
            </fieldset>
        </form>
    </div>
{/block}
