{extends file="parent:frontend/checkout/ajax_add_article.tpl"}


{* Article Name *}
{block name='checkout_ajax_add_information_name'}
    <div class="article--name">
        <ul class="list--name list--unstyled">
            <li class="entry--name">
                <a class="link--name" href="{$detailLink}" title="{$sArticle.articlename|escape}">
                    {$sArticle.articlename|escape|truncate:60}
                </a>
            </li>
            <li>
                <a href="{url controller='listing' action='manufacturer' sSupplier=$sArticle.additional_details.supplierID}"
                   class="supplier--name"
                   title="{$sArticle.additional_details.supplierName}">
                    <p>{$sArticle.additional_details.supplierName|escape|truncate:60}</p>
                </a>
            </li>

            {block name='checkout_ajax_add_information_essential_features'}
                {if {config name=alwaysShowMainFeatures}}
                    <div class="product--essential-features">
                        {$sBasketItem = $sArticle}
                        {include file="string:{config name=mainfeatures}"}
                    </div>
                {/if}
            {/block}
        </ul>
    </div>
{/block}

{* Message for missing cart value for free shipping*}
{block name='checkout_ajax_add_information'}
    {$smarty.block.parent}
    {if $sArticle.shippingfree != 1 && !empty($sArticle.additional_details.supplier_attributes.core)}
        {$freeFrom = $sArticle.additional_details.supplier_attributes.core->get('shippingfreefrom')}
        {if $supplierCartValue < $freeFrom and $supplierShippingCost > 0 and !$isShippingFreeCustomer}
            <div class="message--shipping-free">
                {$shippingDifferenceContent="F&uuml;r weitere <strong>{{$freeFrom - $supplierCartValue}|string_format:"%.2f"|replace:".":","}&euro;</strong> bei <strong>{$sArticle.additional_details.supplierName}</strong> bestellen und die Lieferung <strong>versandkostenfrei</strong> erhalten!"}
                {include file="frontend/_includes/messages.tpl" type="info" content="{$shippingDifferenceContent}"}
            </div>
        {/if}
    {/if}
{/block}

{* Message for missing cart value for premium article*}
{block name='checkout_ajax_add_information'}
    {$smarty.block.parent}
        {if $freePremiumDifference > 0 && $showPromotionModal}
            <div class="message--shipping-free">
                {$premiumDifferenceContent="F&uuml;r weitere <strong>{{$freePremiumDifference}|string_format:"%.2f"|replace:".":","}&euro;</strong> bestellen und einen <b>Primavera Duft Roll-On</b> im Wert von circa 10&euro; erhalten! Der Roll-On ist im Warenkorb ausw&auml;hlbar."}
                {include file="frontend/_includes/messages.tpl" type="info" content="{$premiumDifferenceContent}"}
            </div>
        {/if}
{/block}

{* Shipping free message for customer groups *}
{block name='checkout_ajax_add_information'}
    {$smarty.block.parent}
    {if $sArticle.shippingfree != 1 && $hrCart.groupFree}
        <div class="message--shipping-free">
            {if !$isFullyRegistered && !$hasNewsletterSub}
                    {include file="frontend/_includes/messages.tpl" type="info"
                    content="<strong>{s name="free_shipping_for" namespace="frontend/hr_cart/checkout"}{/s}</strong> <a href='{url controller='account'}'><strong>{s name="register_or_subscribe" namespace="frontend/hr_cart/checkout"}{/s}</strong></a>"}
            {/if}
            {if !$isFullyRegistered && $hasNewsletterSub}
                {include file="frontend/_includes/messages.tpl" type="info"
                content="<strong>{s name="order_free_shipping" namespace="frontend/hr_cart/checkout"}{/s}</strong><a href='{url controller='account'}'><strong>{s name="register" namespace="frontend/hr_cart/checkout"}{/s}</strong></a><strong>{s name="save_shipping_costs_short" namespace="frontend/hr_cart/checkout"}{/s}</strong>"}
            {/if}
            {if $isFullyRegistered && !$hasNewsletterSub}
                {include file="frontend/_includes/messages.tpl" type="info"
                content="<strong>{s name="order_free_shipping" namespace="frontend/hr_cart/checkout"}{/s}</strong><a href='{url controller='account'}'><strong>{s name="subscribe" namespace="frontend/hr_cart/checkout"}{/s}</strong></a><strong>{s name="save_shipping_costs_short" namespace="frontend/hr_cart/checkout"}{/s}</strong>"}
            {/if}
            {if $isShippingFreeCustomer}
                {include file="frontend/_includes/messages.tpl" type="info"
                content="<strong>{s name="free_shipping_message" namespace="frontend/hr_cart/checkout"}{/s}</strong>"}
            {/if}
        </div>
    {/if}
{/block}


{* Cross Selling *}
{block name='checkout_ajax_add_cross_selling'}

    {* Top Seller *}
    {if $showTopProductsModal && $topProductsCategory|@count}
        <div class="modal--cross-selling">
            <div class="panel has--border is--rounded">
                <div class="panel--title is--underline">
                   {$topProductsModalHeadline}
                </div>
                <div class="panel--body">
                    {include file="frontend/_includes/product_slider.tpl" articles=$topProductsCategory}
                </div>

            </div>
        </div>
    {/if}

{/block}