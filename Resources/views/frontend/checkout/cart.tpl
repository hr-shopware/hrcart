{extends file='parent:frontend/checkout/cart.tpl'}


{* Product table content *}
{block name='frontend_checkout_cart_panel'}
    {if $isShippingFreeCustomer}
        {$smarty.block.parent}
    {else}
        <div class="panel has--border">
            <div class="panel--body is--rounded">

                {* Product table header *}
                {block name='frontend_checkout_cart_cart_head'}
                    {include file="frontend/checkout/cart_header.tpl"}
                {/block}

                {* List the products for each supplier *}
                {foreach from=$supplierCartValues key=supplierName item=supplierCartValue}
                    <div>
                        {foreach $sBasket.content as $sBasketItem}
                            {if $supplierName == $sBasketItem.additional_details.supplierName}
                                {if $sBasketItem.modus == 0 && !empty($sBasketItem.additional_details.supplier_attributes.core)}
                                    {$freeFrom = $sBasketItem.additional_details.supplier_attributes.core->get('shippingfreefrom')}
                                {else}
                                    {$freeFrom = 0}
                                {/if}
                                {block name='frontend_checkout_cart_item'}
                                    {include file='frontend/checkout/cart_item.tpl' isLast=$sBasketItem@last}
                                {/block}
                                {if $sShippingcostsDifference}
                                    {$shippingDifferenceContent="<strong>{s name='CartInfoFreeShipping'}{/s}</strong> {s name='CartInfoFreeShippingDifference'}{/s}"}
                                    {include file="frontend/_includes/messages.tpl" type="warning" content="{$shippingDifferenceContent}"}
                                {/if}
                            {/if}
                        {/foreach}

                        {if $supplierCartValue > 0 && $sShippingcosts > 0}
                            <div class="table--tr table--seperator">
                                {if $freeFrom > 0 && $supplierCartValue < $freeFrom && $supplierShippingCosts.$supplierName > 0}
                                    {include file="frontend/_includes/messages.tpl" type="info" content="<strong>{s name='CartInfoFreeShipping'}{/s}</strong> - Noch f&uuml;r weitere <strong>{{$freeFrom-$supplierCartValue}|string_format:"%.2f"|replace:'.':','}&euro;</strong> bei <strong>{$supplierName}</strong> bestellen und die Lieferung versandkostenfrei erhalten!"}
                                {/if}
                            </div>
                        {/if}
                    </div>
                {/foreach}


                 {* premium articles, vouchers, discounts *}
                {foreach $sBasket.content as $sBasketItem}
                    {if $sBasketItem.modus != 0}
                        {block name='frontend_checkout_cart_item'}
                            {include file='frontend/checkout/cart_item.tpl' isLast=$sBasketItem@last}
                        {/block}
                    {/if}
                {/foreach}


                 {* Product table footer *}
                {block name='frontend_checkout_cart_cart_footer'}
                    {include file="frontend/checkout/cart_footer.tpl"}
                {/block}
            </div>
        </div>
    {/if}
{/block}


{block name='frontend_checkout_cart_footer_add_product'}
    {if $hrCart.groupFree}
        {if !$isFullyRegistered && !$hasNewsletterSub}
            {include file="frontend/_includes/messages.tpl" type="info" content="<strong>{s name="free_shipping_for" namespace="frontend/hr_cart/checkout"}{/s}</strong> <a href='{url controller='account'}'><strong>{s name="register_or_subscribe" namespace="frontend/hr_cart/checkout"}{/s}</strong></a><strong>{if $sShippingcosts > 0}{s name="save_shipping_costs" namespace="frontend/hr_cart/checkout"}{/s}{else}{s name="save_shipping_costs_short" namespace="frontend/hr_cart/checkout"}{/s}{/if}</strong>"}
        {/if}
        {if $isFullyRegistered && !$hasNewsletterSub}
            {include file="frontend/_includes/messages.tpl" type="info" content="<strong>{s name="order_free_shipping" namespace="frontend/hr_cart/checkout"}{/s}</strong><a href='{url controller='account'}'><strong>{s name="subscribe" namespace="frontend/hr_cart/checkout"}{/s}</strong></a><strong>{if $sShippingcosts > 0}{s name="save_shipping_costs" namespace="frontend/hr_cart/checkout"}{/s}{else}{s name="save_shipping_costs_short" namespace="frontend/hr_cart/checkout"}{/s}{/if}</strong>"}
        {/if}
        {if !$isFullyRegistered && $hasNewsletterSub}
            {include file="frontend/_includes/messages.tpl" type="info" content="<strong>{s name="order_free_shipping" namespace="frontend/hr_cart/checkout"}{/s}</strong><a href='{url controller='account'}'><strong>{s name="register" namespace="frontend/hr_cart/checkout"}{/s}</strong></a><strong>{if $sShippingcosts > 0}{s name="save_shipping_costs" namespace="frontend/hr_cart/checkout"}{/s}{else}{s name="save_shipping_costs_short" namespace="frontend/hr_cart/checkout"}{/s}{/if}</strong>"}
        {/if}
        {if $isShippingFreeCustomer}
            {include file="frontend/_includes/messages.tpl" type="info" content="<strong>{s name="free_shipping_message" namespace="frontend/hr_cart/checkout"}{/s}</strong>"}
        {/if}
    {/if}
{/block}


{block name="frontend_checkout_footer"}
    {$smarty.block.parent}
    {if $showTopProductsCart && $topProducts|@count}
        <div class="premium-product panel has--border is--rounded slider--top-products">
            {block name='checkout_ajax_add_cross_slider'}
                <div class="topseller--title premium-product--title panel--title is--underline">
                    {$topProductsCartHeadline}
                </div>
                {include file="frontend/_includes/product_slider.tpl" articles=$topProducts}
            {/block}
        </div>
    {/if}
{/block}
