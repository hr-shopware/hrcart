{extends file='parent:frontend/checkout/cart_footer.tpl'}

{* Add product using a voucher *}
{block name='frontend_checkout_cart_footer_add_voucher'}
    <form method="post" action="{url action='addVoucher' sTargetAction=$sTargetAction}" class="table--add-voucher add-voucher--form">

        {block name='frontend_checkout_cart_footer_add_voucher_trigger'}
        {/block}

        {block name='frontend_checkout_cart_footer_add_voucher_label'}
        {/block}

        <div class="add-voucher--panel block-group">
            {block name='frontend_checkout_cart_footer_add_voucher_field'}
                {s name="CheckoutFooterAddVoucherLabelInline" assign="snippetCheckoutFooterAddVoucherLabelInline"}{/s}
                <input type="text" class="add-voucher--field is--medium block" name="sVoucher" placeholder="{$snippetCheckoutFooterAddVoucherLabelInline|escape}" />
            {/block}

            {block name='frontend_checkout_cart_footer_add_voucher_button'}
                <button type="submit" class="add-voucher--button is--medium btn is--primary is--center block">
                    <i class="icon--arrow-right"></i>
                </button>
            {/block}
        </div>
    </form>
{/block}
