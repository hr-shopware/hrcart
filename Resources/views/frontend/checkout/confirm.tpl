{extends file="parent:frontend/checkout/confirm.tpl"}

{block name='frontend_checkout_confirm_item_outer'}
    {$smarty.block.parent}
    {if $hrCart.groupFree}
        {namespace name="frontend/hr_cart/checkout"}
        {if !$hasNewsletterSub && $sShippingcosts > 0}
            {include file="frontend/_includes/messages.tpl" type="info" content="<strong>{s name="order_free_shipping" namespace="frontend/hr_cart/checkout"}{/s}</strong><a href='{if $accountmode === '1'}{url controller='newsletter'}{else}{url controller='account'}{/if}'><strong>{s name="subscribe" namespace="frontend/hr_cart/checkout"}{/s}</strong></a><strong>{if $sShippingcosts > 0}{s name="save_shipping_costs" namespace="frontend/hr_cart/checkout"}{/s}{else}{s name="save_shipping_costs_short" namespace="frontend/hr_cart/checkout"}{/s}{/if}</strong>"}
        {/if}
        {if $isShippingFreeCustomer}
            {include file="frontend/_includes/messages.tpl" type="info" content="<strong>{s name="free_shipping_message" namespace="frontend/hr_cart/checkout"}{/s}</strong>"}
        {/if}
    {/if}
{/block}
