{extends file="parent:frontend/checkout/items/product.tpl"}

{* Product name *}
{block name='frontend_checkout_cart_item_details_title'}

    <a class="content--title" href="{$detailLink}" title="{$sBasketItem.articlename|strip_tags|escape}"
            {if {config name=detailmodal} && {controllerAction|lower} === 'confirm'}
        data-modalbox="true"
        data-content="{url controller="detail" action="productQuickView" ordernumber="{$sBasketItem.ordernumber}" fullPath}"
        data-mode="ajax"
        data-width="750"
        data-sizing="content"
        data-title="{$sBasketItem.articlename|strip_tags|escape}"
        data-updateImages="true"
            {/if}>
        {$sBasketItem.articlename|strip_tags|truncate:60}
    </a>
    <a href="{url controller='listing' action='manufacturer' sSupplier=$sBasketItem.additional_details.supplierID}"
       class="supplier--name"
       title="{$sBasketItem.additional_details.supplierName}">
        <p>{$sBasketItem.additional_details.supplierName}</p>
    </a>
{/block}
