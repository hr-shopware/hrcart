{extends file='parent:frontend/newsletter/index.tpl'}


{block name="frontend_newsletter_headline_info"}
    <p class="newsletter--info">{s name="sNewsletterInfo"}{/s}</p>
    {if $hrCart.groupFree}
        </br>
        <strong style="color:#23405C;">
            {s name="order_free_shipping" namespace="frontend/hr_cart/account"}{/s}
        </strong>
        <p class="newsletter--info">{s name="shipping_free_info_text" namespace="frontend/hr_cart/account"}{/s}
            {s name="shipping_free_info_text_2" namespace="frontend/hr_cart/account"}{/s}</p>
    {/if}
{/block}

{block name="frontend_newsletter_form_additionalfields"}{/block}

{* Required fields hint *}
{block name="frontend_newsletter_form_required"}{/block}
