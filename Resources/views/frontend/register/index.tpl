{extends file="parent:frontend/register/index.tpl"}

{* Captcha *}
{block name='frontend_register_index_form_captcha'}
    {$smarty.block.parent}

    {* Newsletter settings *}
    {block name="frontend_account_index_newsletter_settings_headline"}
        <h2 class="panel--title is--underline">{s name="AccountHeaderNewsletterFreeShipping" namespace="frontend/account/index"}Newsletter abonnieren und Versandkosten sparen{/s}</h2>
    {/block}

    {block name="frontend_account_index_newsletter_settings_content"}
        <div class="panel--body is--wide">

            {* Newsletter checkbox *}
            <div class="register--password-description">
                <input type="checkbox" form="register--form" name="register[personal][newsletter]" id="newsletter" value="1" />

            {* Newsletter label *}
                <label for="newsletter">
                    {s name="AccountLabelWantNewsletter" namespace="frontend/account/index"} Ja, ich möchte den kostenlosen {$sShopname} Newsletter erhalten. Sie können sich jederzeit wieder abmelden!{/s}
                </label>
                <div>
                    </br>
                    <strong style="color:#23405C;">
                        {s name="order_free_shipping" namespace="frontend/hr_cart/account"}{/s}
                    </strong>
                        {s name="shipping_free_info_text" namespace="frontend/hr_cart/account"}{/s}
                        {s name="shipping_free_info_text_2" namespace="frontend/hr_cart/account"}{/s}
                </div>
            </div>
        </div>
    {/block}
{/block}



{block name="frontend_register_index_form_privacy"}
    {if {config name=ACTDPRTEXT} || {config name=ACTDPRCHECK}}
        {block name="frontend_register_index_form_privacy_title"}
            <h2 class="panel--title is--underline">
                {s name="PrivacyTitle" namespace="frontend/index/privacy"}{/s}
            </h2>
        {/block}
        <div class="panel--body is--wide">
            {block name="frontend_register_index_form_privacy_content"}
                <div class="register--password-description">
                    {if {config name=ACTDPRCHECK}}
                        {* Privacy checkbox *}
                        {block name="frontend_register_index_form_privacy_content_checkbox"}
                            <input name="register[personal][dpacheckbox]" type="checkbox" id="dpacheckbox"{if $form_data.dpacheckbox} checked="checked"{/if} required="required" aria-required="true" value="1" class="is--required" />
                            <label for="dpacheckbox">
                                {s name="PrivacyText" namespace="frontend/index/privacy"}{/s}
                            </label>
                        {/block}
                    {else}
                        {block name="frontend_register_index_form_privacy_content_text"}
                            {s name="PrivacyText" namespace="frontend/index/privacy"}{/s}
                        {/block}
                    {/if}
                </div>
            {/block}
        </div>
    {/if}

{/block}
