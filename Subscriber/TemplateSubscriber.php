<?php

namespace HrCart\Subscriber;

use DateTime;
use Enlight\Event\SubscriberInterface;
use Enlight_Event_EventArgs;
use Enlight_Event_Exception;
use Enlight_Exception;
use Enlight_Template_Manager;
use Exception;
use HrGeneral\Services\NewsletterPortal;
use Mailjet\Client;
use Mailjet\Resources;
use Shopware_Components_Modules;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zend_Db_Adapter_Exception;

//Dependencies
//composer require mailjet/mailjet-apiv3-php
//composer require guzzlehttp/guzzle:~5.0

class TemplateSubscriber implements SubscriberInterface
{
    /**
     * @var Enlight_Template_Manager
     */
    private Enlight_Template_Manager $templateManager;

    private string $viewDirectory;

    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    private bool $isShippingFreeCustomer;

    /**
     * @var Shopware_Components_Modules
     */
    private Shopware_Components_Modules $modules;

    /**
     * @var NewsletterPortal
     */
    private NewsletterPortal $newsletterPortal;

    private array $config;

    /**
     * templateSubscriber constructor.
     *
     * @param Enlight_Template_Manager    $templateManager
     * @param                             $viewDirectory
     * @param ContainerInterface          $container
     * @param Shopware_Components_Modules $modules
     * @param NewsletterPortal            $newsletterPortal
     * @param                             $config
     */
    public function __construct(
        Enlight_Template_Manager $templateManager,
        $viewDirectory,
        ContainerInterface $container,
        Shopware_Components_Modules $modules,
        NewsletterPortal $newsletterPortal,
        $config
    ) {
        $this->templateManager        = $templateManager;
        $this->viewDirectory          = $viewDirectory;
        $this->container              = $container;
        $this->isShippingFreeCustomer = false;
        $this->modules                = $modules;
        $this->newsletterPortal       = $newsletterPortal;
        $this->config                 = $config;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'Enlight_Controller_Action_PreDispatch_Frontend_Checkout'        => 'onPreDispatchCheckout',
            'Enlight_Controller_Action_PostDispatchSecure_Frontend'          => 'onPostDispatchFrontend',
            'Enlight_Controller_Action_PostDispatchSecure_Frontend_Checkout' => 'onPostDispatchCheckout',
        ];
    }

    /**
     * Checks if user is completely registered and subscribed to newsletter in portal or mailjet
     * and changes customer group accordingly
     *
     * @param Enlight_Event_EventArgs $arguments
     *
     * @throws Exception
     */
    public function onPreDispatchCheckout(Enlight_Event_EventArgs $arguments): void
    {
        $subject = $arguments->get('subject');

        if (!$subject) {
            return;
        }

        $admin = $this->modules->Admin();

        $view                  = $subject->View();
        $userLoggedIn          = $admin->sCheckUser();
        $userData              = $admin->sGetUserData();
        $userId                = $userData['additional']['user']['userID'];
        $email                 = $userData['additional']['user']['email'];
        $accountmode           = $userData['additional']['user']['accountmode'];
        $shipFreeCustomerGroup = 'VF';
        $defaultCustomerGroup  = 'EK';

        if ($userLoggedIn) {
            $isFullyRegistered = false;
            $hasNewsletterSub  = false;

            if ($this->config['groupFree']) {
                $isFullyRegistered = $this->registrationComplete($userData);
                $hasNewsletterSub  = $this->isSubscribedHrPortal($email) || $this->isSubscribedMailjet($email);
            }

            if ($isFullyRegistered && $hasNewsletterSub && $this->config['groupFree']) {
                if ($userData['additional']['user']['customergroup'] === $defaultCustomerGroup) {
                    $this->setCustomerGroup($userId, $shipFreeCustomerGroup);
                }
                $this->isShippingFreeCustomer = true;
            } elseif ($userData['additional']['user']['customergroup'] === $shipFreeCustomerGroup) {
                $this->setCustomerGroup($userId, $defaultCustomerGroup);
            }

            $view->assign('isShippingFreeCustomer', $this->isShippingFreeCustomer);
            $view->assign('isFullyRegistered', $isFullyRegistered);
            $view->assign('hasNewsletterSub', $hasNewsletterSub);
            $view->assign('accountmode', $accountmode);
        }

        $topProducts = Shopware()->Modules()->Articles()->sGetArticleCharts(3);
        $topProducts = array_slice($topProducts, 0, $this->config['topProductsSliderMaxCount']);
        $view->assign('topProducts', $topProducts);

        $view->assign('topProductsCartHeadline', $this->config['topProductsCartHeadline']);
        $view->assign('topProductsModalHeadline', $this->config['topProductsModalHeadline']);
        $view->assign('showTopProductsModal', $this->config['showTopProductsModal']);
        $view->assign('showTopProductsCart', $this->config['showTopProductsCart']);
    }

    /**
     * @throws Enlight_Exception|Enlight_Event_Exception|Zend_Db_Adapter_Exception
     */
    public function onPostDispatchFrontend(Enlight_Event_EventArgs $arguments): void
    {
        $subject = $arguments->get('subject');
        if (!$subject) {
            return;
        }

        $basket = $this->modules->Basket()->sGetBasket();

        $view        = $subject->View();
        $ordernumber = trim($arguments->getRequest()->getParam('sAdd'));
        $articleId   = $this->modules->Articles()->sGetArticleIdByOrderNumber($ordernumber);
        $articles    = [];

        if ($articleId) {
            $articles        = $this->modules->Articles()->sGetArticleById($articleId);
            $similarArticles = $articles['sSimilarArticles'];
            $view->assign('similarArticles', $similarArticles);

            $topsellerCategory = Shopware()->Modules()->Articles()->sGetArticleCharts($articles['categoryID']);
            $boughtToo         = $view->getAssign('sCrossBoughtToo');
            $recommendations   =
                array_unique(array_merge($similarArticles, $boughtToo, $topsellerCategory), SORT_REGULAR);
            foreach ($basket['content'] as $basketItem) {
                foreach ($recommendations as $key => $recommendation) {
                    if ($basketItem['articleID'] === $recommendation['articleID']) {
                        unset($recommendations[$key]);
                    }
                }
            }
            $recommendations = array_slice($recommendations, 0, $this->config['topProductsSliderMaxCount']);
            $view->assign('topProductsCategory', $recommendations);
        }

        /*Get basket and calculate the cart value of articles from the supplier of this article*/

        $supplierCartValue    = 0;
        $supplierShippingCost = 0;
        $cartValue            = 0;

        foreach ($basket['content'] as $basketItem) {
            if ($basketItem['modus'] === '0'
                && $basketItem['additional_details']['supplierName'] === $articles['supplierName']
            ) {
                $supplierCartValue    += str_replace(",", ".", $basketItem['price']) * $basketItem['quantity'];
                $supplierShippingCost = $basketItem['additional_details']['shippingcost'];
            }
            if ($basketItem['modus'] === '0') {
                $cartValue += str_replace(",", ".", $basketItem['price']) * $basketItem['quantity'];
            }
        }

        $view->assign('isShippingFreeCustomer', $this->isShippingFreeCustomer);

        $view->assign('supplierCartValue', $supplierCartValue);
        $view->assign('supplierShippingCost', $supplierShippingCost);
        $view->assign('hrCart', $this->config);
        $this->templateManager->addTemplateDir($this->viewDirectory);

        $freePremiumDifference = 100.00 - $cartValue;
        $view->assign('freePremiumDifference', $freePremiumDifference);
        $view->assign('showPromotionModal', $this->config['showPromotionModal']);
    }

    public function onPostDispatchCheckout(Enlight_Event_EventArgs $arguments): void
    {
        $subject = $arguments->get('subject');

        if (!$subject) {
            return;
        }

        $view   = $subject->View();
        $basket = $view->getAssign('sBasket');
        $view->addTemplateDir($this->viewDirectory);

        $supplierCartValues    = [];
        $supplierShippingCosts = [];

        foreach ($basket['content'] as $basketItem) {
            if ($basketItem['modus'] === '0') {
                $supplierCartValues[$basketItem['additional_details']['supplierName']] += str_replace(
                        ',',
                        '.',
                        $basketItem['price']
                    ) * $basketItem['quantity'];
                if ($basketItem['shippingfree'] !== 1) {
                    $supplierShippingCosts[$basketItem['additional_details']['supplierName']] += $basketItem['additional_details']['shippingcost'];
                }
            }
        }
        $view->assign('supplierCartValues', $supplierCartValues);
        $view->assign('supplierShippingCosts', $supplierShippingCosts);
        $view->assign('hrCart', $this->config);
    }

    /**
     * Checks if user is completely registered
     *
     * @param $userData
     *
     * @return bool
     */
    private function registrationComplete($userData): bool
    {
        return ($userData['billingaddress']['firstname']
            && $userData['billingaddress']['lastname']
            && $userData['billingaddress']['street']
            && $userData['billingaddress']['zipcode']
            && $userData['billingaddress']['city']
            && $userData['billingaddress']['phone']
            && $userData['additional']['user']['email']
        );
    }

    /**
     * Checks if user subscribed to a contact list in mailjet
     *
     * @param $email
     *
     * @return bool
     */
    private function isSubscribedMailjet($email): bool
    {
        $apiKeyPublic = '88362303826f2dfd4e56751ea2393edd';
        $apiKeySecret = 'ab3f4ff08ec3e9995025643460358b70';
        $isSubscribed = false;

        require __DIR__ . '/../vendor/autoload.php';
        $mj = new Client($apiKeyPublic, $apiKeySecret, true, ['version' => 'v3']);
        $mj->setConnectionTimeout(20);
        $filters = [
            'Unsub'        => false,
            'ContactEmail' => $email,
        ];

        $request = $mj->get(Resources::$Listrecipient, ['filters' => $filters]);

        if ($request->success()) {
            $data = $request->getData();

            if (!empty($data)) {
                $isSubscribed = !$data[0]['IsUnsubscribed'];
            }
        }

        return $isSubscribed;
    }

    /**
     * Checks if user subscribed to portal newsletter in the last 24 hours
     *
     * @param $email
     *
     * @return bool
     * @throws Exception
     */
    private function isSubscribedHrPortal($email): bool
    {
        $result           = $this->newsletterPortal->getSubscriptionDataPortal($email);
        $hasSubscription  = $result['data']['hasNewsletter'];
        $registrationDate = new DateTime($result['data']['registrationDateAndTime']);
        $now              = new DateTime();
        $difference       = $registrationDate->diff($now);

        return $hasSubscription && $difference->days < 1;
    }

    /**
     * Checks if user subscribed shopware newsletter - not used because newsletter status is saved in portal
     *
     * @param $email
     *
     * @return bool
     */
    private function isSubscribedShopware($email): bool
    {
        $connection = $this->container->get('dbal_connection');
        if (!$connection) {
            return false;
        }
        $sql       = 'SELECT * FROM s_campaigns_mailaddresses WHERE email = :email';
        $statement = $connection->prepare($sql);

        $statement->execute(['email' => $email]);
        $result = $statement->fetch();

        return !($result === null);
    }

    /**
     * Change the user's customer group
     *
     * @param $userId
     * @param $groupShortcode
     */
    private function setCustomerGroup($userId, $groupShortcode): void
    {
        $connection = $this->container->get('dbal_connection');
        if (!$connection) {
            return;
        }
        $sql       = 'UPDATE s_user SET customergroup = :groupShortcode WHERE id = :userId';
        $statement = $connection->prepare($sql);
        $statement->execute(
            [
                'groupShortcode' => $groupShortcode,
                'userId'         => $userId,
            ]
        );
    }
}
